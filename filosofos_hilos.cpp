#include <pthread.h>
#include <stdio.h>
#include <iostream>
#include <windows.h>
#include <time.h>
using namespace std;

pthread_mutex_t m=PTHREAD_MUTEX_INITIALIZER;//Gestionador
int rafaga=1000;

int rnd(int min, int max){
	return min+rand()%(max+1-min);
}

class filosofo{
	public:
		int estado=0;
		int tiempo;
		bool palilloLibre=true;
		int x, y;
		filosofo(){}
		filosofo(int s){
			srand(time(NULL));
			//Establecemos coordenadas
			switch(s){
				case 0:setCoordenadas(35,5);break;
				case 1:setCoordenadas(20,9);break;
				case 2:setCoordenadas(28,15);break;
				case 3:setCoordenadas(42,15);break;
				case 4:setCoordenadas(50,9);break;
			}
			//Establecemos tiempo
			this->tiempo=rnd(3,6);
		}
		setCoordenadas(int x, int y){this->x=x;this->y=y;}
};

filosofo f[5];

void cambioColor(string c){
	int v;
	if(c=="azul")v=9;
	else if(c=="verde")v=2;
	else if(c=="rojo")v=12;
	else if(c=="purpura")v=5;
	else v=0;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7*16+v);
}

void gotoxy(int x,int y){ 
	COORD dwPos;  
	dwPos.X = x;  
	dwPos.Y= y;  
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),dwPos);  
}

void printEstado(int x, int y, int s, int tiempo, int filo){
	switch(s){
		case 0:cambioColor("verde");break;
		case 1:cambioColor("rojo");break;
		case 2:cambioColor("azul");break;
		case -1:cambioColor("purpura");break;
		case 3:cambioColor("verde");break;
	}
	gotoxy(x,y);
	switch(s){
		case 0:cout<<"  (°v°)  ";break;
		case 1:cout<<"  (°_°)  ";break;
		case 2:cout<<" \\(°w°)/ ";break;
		case -1:cout<<"  [-_-]´ ";break;
		case 3:cout<<"\\_(^v^)_/";break;
	}
	gotoxy(x+1,y+1);
	//cout<<filo<<">";
	switch(s){
		case 0:cout<<tiempo<<"] Libre    ";break;
		case 1:cout<<tiempo<<"] Hambre   ";break;
		case 2:cout<<tiempo<<"] Comiendo ";break;
		case -1:cout<<tiempo<<"] Pensando ";break;
		case 3:cout<<tiempo<<"] Terminado";break;
	}
	cambioColor("blanco");
	fflush(stdout);
}

bool nadieComiendo(int act){
	for(int a=0;a<5;a++){
		if(a!=act){
			if(f[a].estado==2)
				return false;
		}
	}
	return true;
}

void printFilosofos(filosofo f[]){
	system("cls");
	int aux=0;
	for(int a=0;a<5;a++){
		printEstado(f[a].x,f[a].y,f[a].estado,f[a].tiempo,a);
		aux++;
		if(aux>3)aux=0;
	}
}

void *hilo(void *arg){
	int *args,s,sig;
	args = (int *)arg;
	//Creacion del filosofo
	f[args[0]]=filosofo(args[0]);
	//Proceso
	do{
		pthread_mutex_lock(&m);
		switch(f[args[0]].estado){
			case 0://Libre
				if(rnd(0,2)==0)f[args[0]].estado=1;
				break;
			case 1://Hambre
				if(f[args[0]].palilloLibre==true && nadieComiendo(args[0])==true){
					f[args[0]].palilloLibre=false;
					f[args[0]].estado=2;
					//Buscando otro hilo para comer
					s=rnd(2,3);
					sig=(args[0]+s>5)?sig=args[0]-5+s:args[0]+s;
					if((f[sig].estado==1 || f[sig].estado==-1) && f[sig].tiempo>0){
						f[sig].estado=2;
						f[sig].palilloLibre=false;
					}else{
						s=(s==2)? 3:2;
						if((f[sig].estado==1 || f[sig].estado==-1) && f[sig].tiempo>0){
							f[sig].estado=2;
							f[sig].palilloLibre=false;
							f[sig].tiempo-=1;
						}
					}
				}else f[args[0]].estado=1;
				break;
			case 2://Comiendo
				if(f[args[0]].tiempo-1>=0)f[args[0]].tiempo-=1;
				if(f[args[0]].tiempo<=0)f[args[0]].estado=3;
				else f[args[0]].estado=-1;
				f[args[0]].palilloLibre=true;
				if(f[sig].estado==2){
					if(f[sig].tiempo-1>=0)f[sig].tiempo-=1;
					if(f[sig].tiempo<=0)f[sig].estado=3;
					else f[sig].estado=-1;
					f[sig].palilloLibre=true;
				}
				break;
			case -1://Pensando
				f[args[0]].estado=1;
				if(f[sig].estado==-1)f[sig].estado=1;
				break;
			case 3://Terminado
				f[args[0]].tiempo=0;
				f[args[0]].estado=3;
				break;
		}
		printFilosofos(f);
		Sleep(rafaga);
		pthread_mutex_unlock(&m);
	}while(f[args[0]].tiempo>0);
	pthread_exit(NULL);
}

int main(){
	setlocale(LC_ALL, "spanish");
	srand(time(NULL));
	system ("color 76");
	pthread_t h[5];
	
	//Definicion de hilos y argumentos
	int a1[1]={0},a2[1]={1},a3[1]={2},a4[1]={3},a5[1]={4};
	
	//Creando hilos
	pthread_create(&h[0], NULL, hilo, (void *)a1);Sleep(1000);
	pthread_create(&h[1], NULL, hilo, (void *)a2);Sleep(1000);
	pthread_create(&h[2], NULL, hilo, (void *)a3);Sleep(1000);
	pthread_create(&h[3], NULL, hilo, (void *)a4);Sleep(1000);
	pthread_create(&h[4], NULL, hilo, (void *)a5);Sleep(1000);
	
	//Esperando ejecucion de hilos
	for(int a=0;a<5;a++){pthread_join(h[a], NULL);}
	
	gotoxy(0,20);
	cout<<"Proceso finalizado..."<<endl; fflush(stdout);
	
	return 0;
}
